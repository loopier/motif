Motif {
	classvar <all;
    var <>motif;

	// set motifs with arrays
	*new { arg key, notes=nil, degrees=nil, durs=[1];
		var pat;
		if (notes.isNil && degrees.isNil.not) {
			notes = Motif.degreesToNotes(degrees);
		};
		if (notes.isNil && degrees.isNil) {
			notes = [0];
			degrees = [0];
		};
		pat = (
			notes: notes,
			// degrees: degrees,
			durs: durs,
		);

		pat.debug(key);

		if (key.isNil.not) {
			all.put(key, pat);
		};
		^super.newCopyArgs(pat);
    }

	*degreesToNotes { arg degrees;
		// ^(0..11)[Scale.major.degrees[degrees]];
        ^Motif.asNotes(degrees);
	}

    *asNotes { arg degrees;
        ^Scale.major.degrees[degrees];
    }

	*at { |key|
		^all.at(key)
	}

	*newFromKey { |key|
		var motif = this.at(key).deepCopy;
		motif ?? { ("Unknown motif " ++ key.asString).warn; ^nil };
		^motif;
	}

	*doesNotUnderstand { |selector, args|
		var motif = this.newFromKey(selector, args).deepCopy;
		^motif ?? { super.doesNotUnderstand(selector, args) };
	}

	*names {
		^all.keys.asArray.sort;
	}

	*directory {
		^this.names.collect(_.postln);
	}

	// returns array of given 'size' with default
	// values set to 'defaultValue', and values at
	// 'postitions' set to 'replaceValue'
    *asArray { arg positions, size=16, zeroIndex=false, defaultValue=\r, replaceValue=1;
        var arr = Array.fill(size, defaultValue);
		// zeroIndex.debug("zero");
        positions.do{|x|
			arr.put(x.asInt - zeroIndex.not.asInteger, replaceValue);
			// x.postln;
        }
        ^arr;
    }

	// returns a random motif.
	// \param stepProbability   INT   Probability of notes progressing by step
	// \param leapProbability   INT   Probability of notes progressing by leap
	*rand { arg size=rrand(2,8), notemin=0, notemax=7, stepProbability=10, leapProbability=1, durs=[1,2,4];
		var notes = Array.fill(
			size: size.max(2),
			function: Pwalk(
				list: (notemin..notemax),
				stepPattern: Pwrand(
					list: (notemax.neg..notemax),
					weights: Array.fill((notemax.neg..notemax).size, {|i|
						if((i >= (notemax-1)) && (i <= (notemax+1))) {
							stepProbability
						} {
							leapProbability
						}
					}).postln.normalizeSum,
					repeats: inf),
				startPos: (notemin..notemax).size.rand).iter);
		var motifdurs = Array.fill(size, {durs.choose}).normalizeSum.sort.swap(size.rand, size.rand);
		^Motif(nil, notes:notes, durs:motifdurs);
	}


	print {
		this.motif.keysValuesDo{|k, v| v.debug(k)};
	}

	newFromKey { |key|
		var motif = this.motif.at(key).deepCopy;
		motif ?? { ("Unknown motif " ++ key.asString).warn; ^nil };
		^motif;
	}

	doesNotUnderstand { |selector, args|
		// TODO: !!! any array function is applied to BOTH arrays (degrees, durs)
		var motif = this.newFromKey(selector, args).deepCopy;
		^motif ?? { super.doesNotUnderstand(selector, args) };
	}

	at { arg key;
		^motif.at(key);
	}

	variation { arg level;
		switch (level)
		{0}{^this.notes.debug("original")}
		{1}{^(this.notes + (Array.fill(this.notes.size-1, 0) ++ [-1,1].choose).scramble).debug("variation #"++level)}
		{2}{^(this.notes + (Array.fill(this.notes.size-2, 0) ++ {[-1,1].choose}.dup).scramble).debug("variation #"++level)}
		{3}{^(this.notes * (Array.fill(this.notes.size, {|i| if (i.mod(4)==0){\r}{1}})).scramble).debug("variation #"++level)}
		{4}{^this.notes.swap(this.notes.size.rand, this.notes.size.rand).debug("variation #"++level)}
		// {4}{^this.notes
		// 	.swap(this.notes.size.rand, this.notes.size.rand.debug("variation #"++level))
		// 	.swap(this.notes.size.rand, this.notes.size.rand)
		// .debug("variation #"++level)}
		{5}{^this.notes.reverse.debug("variation #"++level)}
	}

// the motifs

	*initClass {
		all = IdentityDictionary[
            // basic
        \basic -> (
            notes: [0],
            durs: [1],
        ),
        \one -> (
			notes: [ 0, 4, 7 ],
            durs: [1,0.5],
        ),
        \turk -> (
            notes: [ 
                4, 0, 4, 0, 4, 0, 5, 4, 2, 
                4, 0, 4, 0, 4, 0, 5, 4, 2, 
                4, 0, 4, 0, 4, 0, 5, 4, 2, 
                0, 2, 4, 2, 4, 5, 4, 5, 7 ],
            // degrees: [2,0, 2,0, 2,0, 3,2,1,
					// 2,0, 2,0, 2,0, 3,2,1,
					// 2,0, 2,0, 2,0, 3,2,1,
					// 0,1,2, 1,2,3, 2,3,4,
				// ],
            durs: [1/8] * 2,
        ),
        \turkbass -> (
            notes: [ 
                0, 11, 9, 7, 
                0, 7, 9, 11, 
                0, 11, 9, 7, 
                0, 2, 4, 5 ],
            // degrees: [0,6,5,4,
					// 0,4,5,6,
					// 0,6,5,4,
					// 0,1,2,3,
				// ],
            durs: [2/8, 2/8, 2/8, 3/8,
				2/8, 2/8, 2/8, 3/8,
				2/8, 2/8, 2/8, 3/8,
				3/8, 3/8, 3/8,
				] * 2,
        ),
		\hom -> (
			notes: [ 3, 3, 4, 3, 4, 3, 2, 2, 3, 3, 3, 3, 5, 6, 4, 4 ],
			durs: [1],
		),
		\pseq -> (
			notes: [0, 2b, 3],
			durs: [1],
		),
		];

		// all = all.freezeAsParent;
	}

	asPbind {
		^Pbind(\degree, Pseq(this.degrees, inf), \dur, Pseq(this.durs, inf));
	}
}
